


class TikTakToe(){

    fun createGameBoard():MutableMap<Pair<Int,Int>,Char>{
        val gameBoard = mutableMapOf<Pair<Int,Int>,Char>(
            Pair(0,0) to '-',Pair(0,1) to '-',Pair(0,2) to '-',
            Pair(1,0) to '-',Pair(1,1) to '-',Pair(1,2) to '-',
            Pair(2,0) to '-',Pair(2,1) to '-',Pair(2,2) to '-',)


        return gameBoard

    }

    fun enterMove(rawMove: String, expectedValue: Char,gameBoard:Map<Pair<Int,Int>,Char>):Move{

        val valuesMove=rawMove.split(" ")
        if(rawMove.length<5)
            return Move(Pair(0,0),'R')
        if(valuesMove[0].toIntOrNull()!!<3 && valuesMove[1].toIntOrNull()!!<3 && valuesMove[2][0]==expectedValue){
            val move=Move(Pair(valuesMove[0].toInt(),valuesMove[1].toInt()), valuesMove[2][0])
            if(gameBoard[move.key]=='-')
                return move
        }
        return if(valuesMove[2][0]=='E') {Move(Pair(0,0),'E')} else {Move(Pair(0,0),'R')}




    }

    fun evalGame(gameBoard:Map<Pair<Int,Int>,Char>):String{
        //check X and O
        var arr = arrayOf('X', 'O')

        for (letter in arr){
            if(gameBoard[Pair(0,0)]==letter && gameBoard[Pair(0,1)]==letter && gameBoard[Pair(0,2)]==letter) return "$letter wins!"
            if(gameBoard[Pair(1,0)]==letter && gameBoard[Pair(1,1)]==letter && gameBoard[Pair(1,2)]==letter) return "$letter wins!"
            if(gameBoard[Pair(2,0)]==letter && gameBoard[Pair(2,1)]==letter && gameBoard[Pair(2,2)]==letter) return "$letter wins!"
            if(gameBoard[Pair(0,0)]==letter && gameBoard[Pair(1,0)]==letter && gameBoard[Pair(2,0)]==letter) return "$letter wins!"
            if(gameBoard[Pair(0,1)]==letter && gameBoard[Pair(1,1)]==letter && gameBoard[Pair(2,1)]==letter) return "$letter wins!"
            if(gameBoard[Pair(0,2)]==letter && gameBoard[Pair(1,2)]==letter && gameBoard[Pair(2,2)]==letter) return "$letter wins!"
            if(gameBoard[Pair(0,0)]==letter && gameBoard[Pair(1,1)]==letter && gameBoard[Pair(2,2)]==letter) return "$letter wins!"
            if(gameBoard[Pair(0,2)]==letter && gameBoard[Pair(1,1)]==letter && gameBoard[Pair(2,0)]==letter) return "$letter wins!"
        }
        if(gameBoard.count{element -> element.value=='X' || element.value=='O'}==9)
            return "Draw!"

        return "next move"
    }
}

class Move(
    val key:Pair<Int,Int>,
    val value:Char
){}