import org.junit.jupiter.api.Test

class TikTakToeNewBoardTest {
    val gameBoard = TikTakToe().createGameBoard()

    @Test
    fun evalGameBoardSize() {

        assert(gameBoard.size.equals(9))
    }

    @Test
    fun allValuesBeginMinus(){

        assert(gameBoard.all { entry -> entry.value=='-' })
    }

}


