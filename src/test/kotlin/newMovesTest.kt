import org.junit.jupiter.api.Test

class newMovesTest{
    val newMove = TikTakToe().enterMove("0 0 X")

    @Test
    fun checkNewMovesValues(){

        assert(newMove.value.equals('O') or newMove.value.equals('X') )
    }
}
